`timescale 1ns / 1ps
//*************************************************************************
//   > 文件名: adder.v
//   > 描述  ：加法器，直接使用"+"，会自动调用库里的加法器
//   > 作者  : LOONGSON
//   > 日期  : 2016-04-14
//*************************************************************************
module adder2(
    input  [31:0] operand1,
    input  [31:0] operand2,
    input         cin,
    output [31:0] result,
    output        cout
    );
    wire cout1;
    reg cin2;
    initial
    begin
        cin2 = 0;
    end
    always@(cout1)
    begin
        cin2 = cout1;
    end 
    adder1 uut1(operand1[15:0],operand2[15:0],cin,result[15:0], cout1);
    adder1 uut2(operand1[31:16],operand2[31:16],cin2,result[31:16], cout);
    

endmodule
