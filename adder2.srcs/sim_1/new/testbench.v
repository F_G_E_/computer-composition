`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/03/13 09:58:45
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench();

//Inputs
reg[31:0] operand1;
reg[31:0] operand2;
reg cin;

//outputs
wire[31:0] result;
wire cout;

adder2 uut(operand1,operand2,cin,result,cout);
initial begin
operand1 = 0;
operand2 = 0;
cin = 0;
end
always #10 operand1 = $random;
always #10 operand2 = $random;
always #10 cin = {$random}%2;




endmodule
